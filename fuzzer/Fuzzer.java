import java.io.IOException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;


/* a stub for your team's fuzzer */
public class Fuzzer {

    private static final String OUTPUT_FILE = "fuzz.s";
    private static final List<String> validOpcodes = new ArrayList<String>(Arrays.asList("ADD","SUB","MUL","DIV","LDR","STR","MOV"));
    private static final int maxRegistry = 32;
    private static final int maxMemory = 65535;
	private static final int maxPadding = 40000;
	private static final int strategies = 13;
    private static final String alphabet = "1234567890-=`~!@#$%6&*()_+q wertyuiop[]QWERTYUIOP{}|asdfghjkl;ASDFGHJKL:zxcvbnm,./ZXCVBNM<>?\"\'\\";
    public static void main(String[] args) throws IOException {
        FileOutputStream out = null;
        PrintWriter pw = null;
        try {
            out = new FileOutputStream(OUTPUT_FILE);
            pw = new PrintWriter(out);
            Random rg = new Random();
			
            //----------Fuzzer Begins Here----------//
			//-----Decide Strategy-----//
			int strat = rg.nextInt(strategies);
			/***
			 * 0 - Valid Termination
			 * 1 - Memory Overflow
			 * 2 - Registry Overflow
			 * 3 - Offset Overflow
			 * 4 - Line Overflow
			 * 5 - EOF Overflow
			 * 6 - Dirty Read Registry
			 * 7 - Dirty Read Memory
			 * 8 - Pc Overflow
			 * 9 - Int Overflow
			 * 10 - Invalid Operands
			 * 11 - Invalid Name
			 * 12 - Invalid return
			 */
			//-----Decide Strategy-----//
			//-----Apply Strategy-----//
			if (strat == 6){
				pw.println(generateDirtyRegistyRead());
			} 
			else if(strat == 7){
				pw.println(generateDirtyMemoryRead(rg));
			}
			else if(strat ==8){
				if(rg.nextBoolean()){
					pw.println(jmpOverflow(rg));
				}
				else{
					pw.println(jzOverflow(rg));
				}
			}
			if(strat != 8){
				int paddingLines = rg.nextInt(maxPadding);
				pw.println(generateDivideByZero());
				pw.println(generateInstructionComment(rg));
				for(int x=0;x<paddingLines;x++){
					pw.println(generateValidString(rg,paddingLines-x+3,x+3));
				}
			}
			if(strat == 0){
				pw.println(generateValidReturn(rg));
			}
			else if(strat == 1){
				if(rg.nextBoolean()){
					pw.println(generateMemoryUnderflow(rg));
				}
				else{
					pw.println(generateMemoryOverflow(rg));
				}
			}
			else if(strat == 2){
				pw.println(generateRegOverflow(rg));
			}
			else if(strat == 3){
				pw.println(generateOffsetOverFlow(rg));
			}
			else if(strat == 4){
				pw.println(generateLineOverFlow());
			}
			else if(strat == 5){
				pw.println(generateInstructionOverflow());
			}
			else if(strat == 9){
				if(rg.nextBoolean()){
					pw.println(intOverflow());
				}
				else{
					pw.println(intUnderflow());
				}
			}
			else if(strat == 10){
				pw.println(generateInvalidOperands(rg));
			}
			else if(strat == 11){
				for(int x=0;x<20;x++){
					pw.println(generateInvalidFunctionName(rg,generateValidString(rg, 5, 2)));
				}
				for(int x=0;x<5;x++){
					pw.println(generateValidReturn(rg));
				}
			}
			else if(strat == 12){
				pw.println(generateInvalidReturn(rg));
			}
			//-----Apply Strategy-----//
            //----------Fuzzer Ends Here----------//
        }catch (Exception e){
            e.printStackTrace(System.err);
            System.exit(1); 
        }finally{
            if (pw != null){
                pw.flush();
            }
            if (out != null){
                out.close();
            }
        }

    }

    //-----------Generation Functions----------//
    public static String generateMemoryOverflow(Random rg) {
    	String line = new String();
    	line = line +"MOV R0 "+maxMemory+"\n";
    	line = line +"MOV R1 1 \n";
    	if(rg.nextBoolean()) {
    		line = line +"LDR R0 R1 1";
    	} else {
    		line = line +"STR R0 R1 1";
    	}
    	return line;
    }
    
    public static String generateMemoryUnderflow(Random rg) {
    	String line = new String();
    	line = line +"MOV R0 "+(maxMemory*-1)+"\n";
    	line = line +"MOV R1 1 \n";
    	if(rg.nextBoolean()) {
    		line = line+"LDR R0 R1 -1";
    	} else {
    		line = line+"STR R0 R1 -1";
    	}
    	return line;
    }
    
    public static String generateRegOverflow(Random rg) {
    	String line = new String();
    	int invalidRegistry;
    	Boolean overflow = rg.nextBoolean();
    	if(overflow) {
    		invalidRegistry = 32;
    	}
    	else {
    		invalidRegistry = -1;
    	}
    	int goodRegA;
    	int goodRegB;
    	int pos;
    	int val;
    	switch(rg.nextInt(7)){
    		case(0):
    			goodRegA = rg.nextInt(maxRegistry);
    			goodRegB = rg.nextInt(maxRegistry);
    			line = line +"ADD ";
    			pos = rg.nextInt(3);
    			
    			switch(pos) {
    				case(0):
    					line = line +"R"+invalidRegistry+" R"+goodRegA+" R"+goodRegB;
    					break;
    				case(1):
    					line = line +"R"+goodRegA+" R"+invalidRegistry+" R"+goodRegB;
    					break;
    				case(2):
    					line = line +"R"+goodRegB+" R"+goodRegA+" R"+invalidRegistry;
    					break;
    			}
    			return line;
    		case(1):
    			goodRegA = rg.nextInt(maxRegistry);
    			goodRegB = rg.nextInt(maxRegistry);
    			line = line +"SUB ";
    			pos = rg.nextInt(3);
    			
    			switch(pos) {
    				case(0):
    					line = line +"R"+invalidRegistry+" R"+goodRegA+" R"+goodRegB;
    					break;
    				case(1):
    					line = line +"R"+goodRegA+" R"+invalidRegistry+" R"+goodRegB;
    					break;
    				case(2):
    					line = line +"R"+goodRegB+" R"+goodRegA+" R"+invalidRegistry;
    					break;
    			}
    			return line;
    		case(2):
    			goodRegA = rg.nextInt(maxRegistry);
    			goodRegB = rg.nextInt(maxRegistry);
    			line = line +"MOV R"+goodRegB+" 1\n";
    			line = line +"DIV ";
    			pos = rg.nextInt(3);
    			
    			switch(pos) {
    				case(0):
    					line = line +"R"+invalidRegistry+" R"+goodRegA+" R"+goodRegB;
    					break;
    				case(1):
    					line = line +"R"+goodRegA+" R"+invalidRegistry+" R"+goodRegB;
    					break;
    				case(2):
    					line = line +"R"+goodRegB+" R"+goodRegA+" R"+invalidRegistry;
    					break;
    			}
    			return line;
    		case(3):
    			goodRegA = rg.nextInt(maxRegistry);
    			goodRegB = rg.nextInt(maxRegistry);
    			line = line +"MUL ";
    			pos = rg.nextInt(3);
    			
    			switch(pos) {
    				case(0):
    					line = line +"R"+invalidRegistry+" R"+goodRegA+" R"+goodRegB;
    					break;
    				case(1):
    					line = line +"R"+goodRegA+" R"+invalidRegistry+" R"+goodRegB;
    					break;
    				case(2):
    					line = line +"R"+goodRegB+" R"+goodRegA+" R"+invalidRegistry;
    					break;
    			}
    			return line;
    		case(4):
    			line = line +"LDR ";
    			goodRegA = rg.nextInt(maxRegistry);
    			pos = rg.nextInt(2);
    			val = rg.nextInt(maxMemory);
    			switch(pos) {
				case(0):
					line = line +"R"+invalidRegistry+" "+val+"R"+goodRegA;
					break;
				case(1):
					line = line +"R"+goodRegA+" "+val+"R"+invalidRegistry;
					break;
    			}
    			return line;
    		case(5):
    			line = line +"STR ";
				goodRegA = rg.nextInt(maxRegistry);
				pos = rg.nextInt(2);
				val = rg.nextInt(maxMemory);
				switch(pos) {
				case(0):
					line = line +"R"+invalidRegistry+" "+val+"R"+goodRegA;
					break;
				case(1):
					line = line +"R"+goodRegA+" "+val+"R"+invalidRegistry;
					break;
				}
				return line;
    		case(6):
    			line = line +"MOV ";
    			goodRegA = rg.nextInt(maxRegistry);
    			val = rg.nextInt(maxMemory);
    			line = line +"R"+invalidRegistry+" "+val;
    			return line;
    	}	
    	return line;
    }

    public static String generateOffsetOverFlow(Random rg) {
    	String line = new String();
    	Boolean positive = rg.nextBoolean();
    	if(!positive) {
        line = line +"MOV R0 -70000";	
    	}else {
    	line = line +"MOV R0 70000";
    	}
    	return line;
    }
    
    public static String generateLineOverFlow() {
    	String line = new String();
    	line = line +"RET R0";
    	for(int x=0;x<2000;x++) {
    		line = line +"a";
    	}
    	return line;
    }
    
    public static String generateInstructionOverflow() {
    	String line = new String();
    	line = line +"MOV R0 0\n";
    	line = line +"MOV R1 1";
    	for(int x=0;x<70000;x++) {
    		line = line +"\nADD R0 R0 R1";
    	}
    	return line;
    }
    
    public static String generateDivideByZero(){
    	String line = new String();
    	line = line +"MOV R0 0\n";
    	line = line +"MOV R1 1\n";
    	line = line +"DIV R2 R1 R0\n";
    	return line;
    }
    
    public static String generateDirtyRegistyRead() {
    	String line = new String();

    	for(int x=1;x<maxRegistry;x++) {   
    		line = line +"ADD R0 R0 R"+x+"\n";
    	}
    	line = line +"RET R0";
    	return line;
    }
    
    public static String generateDirtyMemoryRead(Random rg) {
    	String line = new String();
    	int attempts = 20;
    	line = line +"MOV R0 0";
    	for(int x=0;x<attempts;x++) {   
    		int value = rg.nextInt(maxMemory);
    		if(rg.nextBoolean()) {
    			value = value*-1;
    		}
    		line = line +"\nLDR R1 R0 "+value+"\n";
    		line = line +"ADD R2 R2 R1\n";
    	}
		line = line+"RET R2";
    	return line;
    }

    public static String jmpOverflow(Random rg) {
    	String line = new String();
		int val;
		int offset = rg.nextInt(10);
		if(offset == 0){
			offset = 10;
		}
    	if(rg.nextBoolean()) {
    		val = -1*offset;
    	} else {
    		val = offset;
    	}
    	line = line +"JMP "+val;
    	return line;
    }
    
    public static String jzOverflow(Random rg) {
    	
    	String line = new String();
    	int val;
		int offset = rg.nextInt(10);
		if(offset == 0){
			offset = 10;
		}
    	if(rg.nextBoolean()) {
    		val = -1*offset;
    	} else {
    		val = offset;
    	}
    	line = line +"MOV R0 0\n";
    	line = line +"JZ R0 "+val;
    	return line;
    }
    
    public static String intOverflow() {
    	String line = new String();
    	line = line +"MOV R0 65535\n";
    	line = line +"MUL R1 R0 R0\n";
    	line = line +"MUL R1 R0 R0";
    	return line;
    }
    
    public static String intUnderflow() {
    	String line = new String();
    	line = line +"MOV R0 -65535\n";
    	line = line +"MUL R1 R0 R0\n";
    	line = line +"MUL R1 R0 R0";
    	return line;
    }
    
    public static String generateInvalidOperands(Random rg) {
    	int regs = rg.nextInt(4);
    	String line = new String();
    	Boolean hasval = rg.nextBoolean();
    	int index = rg.nextInt(validOpcodes.size());
        String opcode = validOpcodes.get(index);
        line = line +opcode;
        for(int x=0;x<regs;x++) {
        	int register = rg.nextInt(maxRegistry);
        	line = line +" R"+register;
        }
        if(hasval) {
        	int value=rg.nextInt(maxMemory);
        	line = line +" "+value;
        }

    	//too many arguments
    	return line;
    }
    
    public static String generateInvalidFunctionName(Random rg,String line) {
    	char[] charLine = line.toCharArray();
    	System.out.println("line"+line);
    	int change = rg.nextInt(3)+1;
    	System.out.println("change"+change);
    	for(int x=0;x<change;x++) {
    		int index = rg.nextInt(charLine.length);
    		char character = alphabet.charAt(rg.nextInt(alphabet.length()));
    		System.out.println("char at:"+x+" is "+character);
    		charLine[index] = character;
    	}
    			
    	return new String(charLine);
    }

    public static String generateInstructionComment(Random rg) {
    	String line = new String();
    	line = line +";";
    	String instr = generateValidReturn(rg);
    	line = line +instr;
    	return line;
    }
    
    public static String generateValidReturn(Random rg) {
    	String line = new String();
    	line = line +"RET";
    	line = line +" R"+rg.nextInt(maxRegistry);
    	return line;
    }
    
    public static String generateInvalidReturn(Random rg) {
    	int invalidRegistry;
    	String line = new String();
    	line = line +"RET";
    	Boolean overflow = rg.nextBoolean();
    	if(overflow) {
    		invalidRegistry = 32;
    	}
    	else {
    		invalidRegistry = -1;
    	}
    	
    	line = line +" R"+invalidRegistry;
    	return line;
    }
    
    public static String generateValidString(Random rg,int programLength,int lineNumber){
        int index = rg.nextInt(validOpcodes.size());
        String opcode = validOpcodes.get(index);
        int numregs = 0;
        Integer offset = null;
        String line = new String();
        switch(opcode){
            case("ADD"):
                numregs = 3;
                break;
            case("SUB"):
                numregs = 3;
                break;
            case("MUL"):
                numregs = 3;
                break;
            case("DIV"):
                numregs = 3;    
                break;
            case("LDR"):
                numregs = 2;
            	offset = (rg.nextInt(2*maxMemory))-maxMemory;
                break;
            case("STR"):
                //special case - form <REGISTER VALUE REGISTER>
            	offset = (rg.nextInt(2*maxMemory))-maxMemory;
            	line = line +opcode;
                line = line +" R"+rg.nextInt(maxRegistry);
                line = line +" "+offset;
                line = line +" R"+rg.nextInt(maxRegistry);
                return line;
            case("MOV"):
                numregs = 1;
            	offset = rg.nextInt(maxMemory);
            	Boolean positive = rg.nextBoolean();
            	if(!positive) {
            		offset = -offset;
            	}
                break;
            case("JMP"):
                //special case - avoid looping infinitly
                numregs = 0;
            	offset = (rg.nextInt(programLength));
            	offset = offset - lineNumber;
            	if(offset<0) {
            		offset = offset-1;
            		line = line +"JMP 2\n";
            	} else if(offset == 0) {
            		offset = offset+1;
            	}
            	
                break;
            case("JZ"):
                //special case - avoid looping infinitly
                numregs = 1;
            	if(rg.nextBoolean()) {
            		offset = -1;
            	}
        		offset = (rg.nextInt(programLength));
        		offset = offset - lineNumber;
        		if(offset<0) {
        			offset = offset-2;
        			line = line +"JMP 2\n";
        		} else if(offset == 0) {
        			offset = offset+1;
        		}
                break;
        }
        line = line +opcode;
        int x;
        for(x=0;x<numregs;x++){
        	line = line +" R"+rg.nextInt(maxRegistry);
        }
        if(offset!=null){
            line = line +" "+offset;
        }
        return line;
    }

}
